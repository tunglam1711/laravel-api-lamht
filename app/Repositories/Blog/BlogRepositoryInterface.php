<?php

namespace App\Repositories\Blog;

use App\Repositories\RepositoryInterface;

interface BlogRepositoryInterface extends RepositoryInterface
{
    /**
     * Get number of blog
    */
    public function getBlog();

    /**
     * search blog by title
     * @param $title
     */
    public function searchBlog($param);
}
