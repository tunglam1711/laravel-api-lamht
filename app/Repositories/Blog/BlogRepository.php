<?php

namespace App\Repositories\Blog;

use App\Models\Blog;
use App\Repositories\BaseRepository;

class BlogRepository extends BaseRepository implements BlogRepositoryInterface
{

    /**
     * get model
     * @return mixed
    */
    public function getModel()
    {
        return Blog::class;
    }

    /**
     * get blog
     * @return mixed
    */
    public function getBlog()
    {
        return $this->model->select('title')->get();
    }

    // /**
    //  * search blog by title
    //  * @param $title
    //  * @return mixed
    //  */
    public function searchBlog($param){
        return $this->model->where('title','like','%'.$param.'%')->get();
    }
}
