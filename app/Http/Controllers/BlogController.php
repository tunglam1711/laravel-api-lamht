<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blog\BlogRequest;
use App\Http\Resources\BlogResource;
use App\Repositories\Blog\BlogRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    protected $blogRepo;

    public function __construct(BlogRepository $blogRepo)
    {
        $this->blogRepo = $blogRepo;
    }

    /**
     * Display a listing of the resource.
     * @return mixed
     */
    public function index()
    {
        $param = request()->input('param');

        if ($param) {
            $blogs = $this->blogRepo->searchBlog($param);
        } else {
            $blogs = $this->blogRepo->getAll();
        }
        return BlogResource::collection($blogs);
    }

    /**
     * Get info of blog
     * @param $blog
     * @return mixed
     */
    public function show($blog)
    {
        $result = $this->blogRepo->find($blog);
        return new BlogResource($result);
    }

    /**
     * Store a newly created resource in storage.
     * @param BlogRequest $request
     * @return mixed
     */
    public function store(BlogRequest $request)
    {
        $dataStore = $request->all();
        $dataStore['position'] = json_encode($request->position);
        $blog = $this->blogRepo->create($dataStore);
        return new BlogResource($blog);
    }

    /**
     * Update the specified resource in storage.
     * @param BlogRequest $request
     * @param string $blog
     * @return mixed
     */
    public function update(BlogRequest $request, string $blog)
    {
        $dataUpdate = $request->all();
        $dataUpdate['position'] = json_encode($request->position);
        $blogUpdate = $this->blogRepo->update($blog, $dataUpdate);
        return new BlogResource($blogUpdate);
    }

    /**
     * Remove the specified resource from storage.
     * @param string $blog
     * @return mixed
     */
    public function destroy(string $blog)
    {
        $this->blogRepo->delete($blog);
        return response()->json(['message' => 'Deleted!!!']);
    }
}
