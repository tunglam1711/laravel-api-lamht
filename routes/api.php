<?php

use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('blogs')->controller(BlogController::class)->group(function () {
   Route::get('/', 'index');
   Route::post('/', 'store');
   Route::get('/{id}', 'show');
   Route::put('/{id}', 'update');
   Route::delete('/{id}', 'destroy');
});
